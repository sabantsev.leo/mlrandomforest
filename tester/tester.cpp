#include <Python.h>


static PyObject* magic(PyObject* self, PyObject* args)
{
    double answer;
    PyArg_ParseTuple(args, "d", &answer);
    if (PyErr_Occurred())
        return NULL;

    if (answer > 0.97 || answer < 0.90)
        return Py_False;

    return Py_True;
}


static PyMethodDef myMethods[] = {
        {"magic", magic, METH_VARARGS, "Gives an magic answer"},
        {NULL, NULL, 0, NULL}
};


static PyModuleDef myModule = {
        PyModuleDef_HEAD_INIT,
        "AwesomeTester",
        "This is a module for testing homework",
        -1,
        myMethods
};


PyMODINIT_FUNC PyInit_AwesomeTester(void)
{
    PyObject* module = PyModule_Create(&myModule);
    return module;
}