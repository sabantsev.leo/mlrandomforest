from distutils.core import setup
from distutils.extension import Extension

module = Extension(
    'AwesomeTester',
    sources=['tester.cpp'],
    extra_compile_args=["-std=c++17", "-Ofast"],
    language="c++"
)

setup(
    name='AwesomeTester',
    version='1.0.0',
    description="This is a package for testing homework",
    ext_modules=[module])
